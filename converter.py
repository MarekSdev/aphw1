"""
Converter script.

A script using pyproj library to converts coordinates from WGS84 coordinates to
L-Est97 and vice versa.

"""

from pyproj import Transformer


def toWGS(x, y):
    """Convert L-Est97 coordinates to WGS84 coordinates."""
    l_est97_to_wgs84_transform = Transformer.from_crs('epsg:3301', 'epsg:4326')
    return l_est97_to_wgs84_transform.transform(x, y)


def toEST(x, y):
    """Convert WGS84 coordinates to L-Est97 coordinates."""
    wgs84_to_l_est97_transform = Transformer.from_crs('epsg:4326', 'epsg:3301')
    return wgs84_to_l_est97_transform.transform(x, y)
