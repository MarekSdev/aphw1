"""
Converter Application.

Convert coordinates from WGS84 coordinates to L-Est97 and vice versa.
"""
__author__ = "Marek S"
__version__ = "1.0"
__email__ = "msalur@ttu.ee"

from tkinter import messagebox, Menu, Canvas, Tk, PhotoImage, \
    Label, Button, Entry, END, DISABLED
import webbrowser
from converter import toWGS, toEST

toEst = True
yVal = 80
open = True
x = 537731.1
y = 6584335.6


def aboutBox():
    """About Menu messagebox."""
    messagebox.showinfo(
        'About',
        'Convert WGS84 coordinates to L-Est97 and vice versa.\n' +
        'Homework in the subject of Advanced Python.\n' +
        'Developed by MarekS.')


def updateUI():
    """Update UI elements based on selected conversion type."""
    global toEst
    if toEst:
        toEst = False
        toWGSUI()
    else:
        toEst = True
        toLESTUI()

# UI HELPER METHODS


def toWGSUI():
    """Change UI elements text to WGS conversion format."""
    latLbl.config(text="UTM Easting", justify='center')
    longLbl.config(text="UTM Northing", justify='center')
    convTypeBtn.config(text="EST97 > WGS84")


def toLESTUI():
    """Change UI elements text to EST conversion format."""
    latLbl.config(text="Latitude", justify='center')
    longLbl.config(text="Longitude", justify='center')
    convTypeBtn.config(text="WGS84 > EST97")

# CONVERT & SHOW


def calculate(x, y):
    """Convert via converter methods based on conversion type.

    Parameters
    ----------
    x : float
        Lat/UTM Easting value.
    y : float
        Long/UTM Northing value.

    Returns
    -------
    tuple
        Tuple combining lat/long infotmation.

    """
    if toEst:
        return toEST(x, y)
    else:
        setOnlineMapCoords(y, x)
        return toWGS(x, y)


def resultUI(x, y):
    """Show conversion results in GUI.

    Parameters
    ----------
    x : float
        Lat/UTM value.
    y : float
        Long/UTM value.

    """
    infoLbl.config(text='RESULT', justify='center', font="TkSmallCaptionFont")
    conversion = calculate(x, y)
    if toEst:
        setOnlineMapCoords(round(conversion[1], 1), round(conversion[0], 1))
    if toEst:
        resultLbl.config(
            text='Latidude: ' +
            str(conversion[0]) + ' , Longitude: ' + str(conversion[1]),
            fg="black", justify='center')
    else:
        resultLbl.config(
            text='UTM Easting: ' +
            str(conversion[0]) + ' , UTM Northing: ' + str(conversion[1]),
            fg="black", justify='center')
    clearEntryFields()
    showWebBtn["state"] = "normal"

# HELPER FUNCTIONDS


def OpenUrl():
    """Open coordinates in web."""
    global x, y
    webbrowser.open_new(
        'https://xgis.maaamet.ee/xGIS/XGis?app_id=UU82&user_id=at&punkt=' +
        str(x) + ',' + str(y) + '&zoom=95.2690129536204&LANG=2')


def setOnlineMapCoords(lat, long):
    """Set coordinates for url."""
    global x, y
    x = lat
    y = long


def populateDemoData():
    """Populate fields for demo."""
    latField.insert(0, "59.39575569571347")
    longField.insert(0, "24.66492343323389")


def clearEntryFields():
    """Empty entry fields."""
    latField.delete(0, END)
    longField.delete(0, END)


"""APP MAIN SECTION (GUI)"""
# INIT
window = Tk()
window.title("GeoAPP")

# MENU
menu = Menu(window)
new_item = Menu(menu)
new_item.add_command(label='Converter')
new_item.add_separator()
new_item.add_command(label='About', command=aboutBox)
menu.add_cascade(label='GeoApp', menu=new_item)
window.config(menu=menu)

# BACKGROUND CANVAS
C = Canvas(window, bg="blue", height=250, width=300)
filename = PhotoImage(file="img/bg.png")
background_label = Label(window, image=filename)
background_label.place(x=0, y=0, relwidth=1, relheight=1)
C.place(x=0, y=0, relwidth=1, relheight=1)

# APPLICATION TITLE
title = Label(window, text="Coordinate Converter")
title.place(x=320 - title.winfo_reqwidth() / 2, y=0)

# CONVERT TYPE BUTTON
convTypeBtn = Button(window, text='WGS84 > EST97', font=(
    'Helvetica', 14, 'bold'), width=25, command=updateUI)
convTypeBtn.place(x=320 - convTypeBtn.winfo_reqwidth() / 2, y=yVal + 10)

# FIRST AREA
latLbl = Label(window, text="Latitude", fg="gray", width=10, justify='center')
latLbl.place(x=320 - latLbl.winfo_reqwidth() / 2,
             y=yVal + 60 - latLbl.winfo_reqheight() / 2)
latField = Entry(window, width=30, justify='center')
latField.place(x=320 - latField.winfo_reqwidth() / 2,
               y=yVal + 80 - latField.winfo_reqheight() / 2)

# SECOND AREA
longLbl = Label(window, text="Longitude", fg="gray",
                width=10, justify='center')
longLbl.place(x=320 - longLbl.winfo_reqwidth() / 2,
              y=yVal + 110 - longLbl.winfo_reqheight() / 2)
longField = Entry(window, width=30, justify='center')
longField.place(x=320 - longField.winfo_reqwidth() / 2,
                y=yVal + 130 - longField.winfo_reqheight() / 2)

# CONVERT BUTTON
convertBtn = Button(window,
                    text='Convert', width=25,
                    font=('Helvetica', 14, 'bold'),
                    command=lambda: resultUI(latField.get(), longField.get()))
convertBtn.config(bg='red', fg='red4')
convertBtn.place(x=320 - convertBtn.winfo_reqwidth() / 2, y=yVal + 160)

# RESULT AREA
infoLbl = Label(window, text="", fg="black", justify='center',
                width=10, font="TkSmallCaptionFont")
infoLbl.place(x=320 - infoLbl.winfo_reqwidth() / 2,
              y=280 - infoLbl.winfo_reqheight() / 2)
resultLbl = Label(window, text='', fg="black", justify='center', width=55)
resultLbl.place(x=320 - resultLbl.winfo_reqwidth() / 2,
                y=300 - resultLbl.winfo_reqheight() / 2)

# WEB BUTTON
showWebBtn = Button(window, text='SHOW ON WEB',
                    font="TkSmallCaptionFont",  width=25,
                    state=DISABLED, command=OpenUrl)
showWebBtn.place(x=320 - showWebBtn.winfo_reqwidth() / 2, y=340)


# FOOTER
copyrightLbl = Label(window, text="Developed by MarekS", fg="gray")
copyrightLbl.place(x=320 - copyrightLbl.winfo_reqwidth() / 2, y=380)

window.geometry('640x400')
window.mainloop()
