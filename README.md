![icon](img/icon.png =100x100)

# GeoApp (advanced python hw1)

## General information

Locations on earth are often expressed in geographic degrees (latitude and longitude). But when you are surveying you need to talk in meters (or feet). This is because - depending on the application - you use a geographic or projected coordinate system.

A geographic coordinate system (GCS) is a coordinate system which uses a three-dimensional spherical surface (ellipsoid) to define locations on the earth. A common choice of coordinates is latitude and longitude.

In a projected coordinate system (PCS) you project the geographic coordinate that you have measured, to, for example, a cylinder which you roll out easily on two-dimensional surface (the map).

## Application overview

Simple python application that converts coordinates from WGS84 coordinates to L-Est97 and vice versa.

### Completed

- [x] Conversion (using pyproj)
- [x] GUI (tkinter)
- [x] Tests
- [x] Open coordinates in web
- [x] Validation (pep8, pep257, pylint, flake, pylama)

### Screenshot

<img src="img/screenshot.png" alt="menu.png" width="90%"/>

### TODO

- [ ] Add try/catches to validate user input, etc..
- [ ] Swap conversion type button with two drop-downs (add more formats)
- [ ] Implement demo data feature for testing (to avoid retyping)
- [ ] GUI tweaks

## How to use

### Installation

This guide commands are written in python3. (You can also use python instead of python3)

**Requirements**

In order to use this application you need to have **_pyproj_** installed on your system.
OPen terminal and run the following command to install required package to your system.

```
pip install pyproj
```

**Running the application**

Open terminal and navigate to the application root folder. To launch the GUI version of this app run the following command.

```
python3 app.py
```

**Running the unittests**

Open terminal and navigate to the application root folder. To run tests, use the following command.

```
python3 tests.py
```

**Using the Application**

Look above how to run the application.
In the application main window you can choose the conversion type (EST>WSG or vice versa).
Enter the cooridnates and press Convert.
You also have option to view the location on map (simply press "show on web" button).

## Author

- **MarekS** (msalur@ttu.ee)

## License

This project is licensed under the MIT License.
Test
