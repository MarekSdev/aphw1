"""Unittests for converter."""
import unittest
from converter import toWGS, toEST


class TestConversion(unittest.TestCase):
    """Conversion tests."""

    def test_toWGS(self):
        """Test to WGS conversion."""
        conversion = toWGS(6584388.5, 537777.1)
        self.assertEqual(conversion, (59.39575569571347, 24.66492343323389))

    def test_toEST(self):
        """Test to EST conversion."""
        conversion = toEST(59.39575569571347, 24.66492343323389)
        self.assertEqual(conversion, (6584388.499999826, 537777.1000000018))


if __name__ == "__main__":
    unittest.main()
